
// Assignment 2 - Simple Calculator
// Brendan Skelton

#include <iostream>
#include <conio.h>

using namespace std;

float num1;
float num2;
float sum1;
float sum2;
float sum3;
float answer;

float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num, float den, float &ans);

int main()
{
	char choice;
	do {

		char symbol;

		cout << "Enter two positive numbers: ";
		cin >> num1;
		cin >> num2;

		cout << "Enter either a +, -, *, or /: ";
		cin >> symbol;

		switch (symbol)
		{
		case '+':
			Add(float(num1), float(num2));
			cout << sum1 << "\n";
			break;
		case '-':
			Subtract(float(num1), float(num2));
			cout << sum2 << "\n";
			break;
		case '*':
			Multiply(float(num1), float(num2));
			cout << sum3 << "\n";
			break;
		case '/':
			if (Divide(num1, num2, answer))
			{
				cout << answer << "\n";
			}
			else
			{
				cout << "You cannot divide by zero.\n";
			}
			break;
		}
		cout << "If you'd like to do another one, enter \"y\" If you would like to exit, enter anything else: ";
		cin >> choice;
	} while (choice == 'y');

	_getch();
	return 0;
}


float Add(float num1, float num2) 
{
	return sum1 = float(num1) + float(num2);
}

float Subtract(float num1, float num2)
{
	return sum2 = float(num1) - float(num2);
}

float Multiply(float num1, float num2) 
{
	return sum3 = float(num1) * float(num2);
}

bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0) return false;

	answer = num1 / num2;

	num1++;
	num2 += 100;

	return true;
}